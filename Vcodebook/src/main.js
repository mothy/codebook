import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from './App.vue'
import principal from './principal.vue'
import foro from './foro.vue'
import VueResource from 'vue-resource'
import VueSweetalert2 from 'vue-sweetalert2';
import VueRouter from 'vue-router'
import profile from './profile.vue'
import posts from './components/posts.vue'
import perfil from './components/perfil.vue'
import VueSession from 'vue-session'


Vue.config.productionTip = false
Vue.use(VueSweetalert2)
Vue.use(BootstrapVue)
Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(VueSession)


const routes = [{
    path: '/',
    name:'root',
    redirect:'/home'
  },
  {
    path: '/foro/:idForo',
    name:'foro',
    component: foro
  },
  {
    path:'/home',
    name:'home',
    component: principal
  },
  {
    path:'/profile/:idUser',
    name:'profile',
    component: profile,
    children: [
      { path: 'perfil/:id', component: perfil },
      { path: 'posts/:id', component: posts }
    ]
  },
  {
    path:'/perfil',
    component: perfil
  },
  {
    path:'/posts',
    component: posts
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})
Vue.http.options.xhr = {
  withCredentials: true
};

Vue.http.interceptors.push(function(request,next){
  //console.log(request); 
  //development
  request.url = "http://127.0.0.1/codebook/Scodebook"+request.url;
  //production
  //request.url = "http://codebook.goriwd.com"+request.url;
  request.headers.set('X-Requested-with','XMLHttpRequest');
  next(function (response) {
      //console.log('status: ' + response.data)
  return response;
  });
});

var mix = Vue.mixin({
  data:function(){
    return{
      userFlag:false
    }
  }
})


new Vue({
  router,
  mix,
  render: h => h(App)
}).$mount('#app')

