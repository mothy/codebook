<?php
session_cache_limiter(false);
session_start();
date_default_timezone_set('America/Mexico_City');
// CONFIG
define('SYS_TITLE', 'CB[]');
define('SYS_NAME', 'codebook');
define('SYS_DEBUG', true);

// FOLDERS
define('ASSETS_DIR', 'html/');
define('CONTROLLERS_DIR', 'controllers/');
define('VIEWS_DIR', 'views/');
define('MODELS_DIR', 'Modelos/');

define('DBNAME','codebook');
define('DBHOST','localhost');
define('DBUSER','admin');
define('DBPASS','123');
#define('OPCIONES', array(PDO::PGSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
// Slim Vars
define('SLIM_MODE','development');//development,production


$app = new \Slim\Slim();


$app->config(array(
  'debug' => true,
  'templates.path' => VIEWS_DIR,
  'mode' => SLIM_MODE
));

$app->configureMode('production', function () use ($app) {
  $app->config(array('log.enabled' => true, 'debug' => false));
});

$app->configureMode('development', function () use ($app) {
  $app->config(array('log.enabled' => false, 'debug' => true));
});

$app->notFound(function () use ($app) {
  echo "error 404";
});
$app->error(function (\Exception $e) use ($app) {
  echo "Error 500";
});

$app->session = $_SESSION;
$app->flash = (isset($_SESSION['slim.flash'])) ? $_SESSION['slim.flash'] : null;

$rootUri = $app->request()->getRootUri();
$assetUri = $rootUri;
$resourceUri = $_SERVER['REQUEST_URI'];
$parts = explode("/",$app->request()->getPathInfo());

// DATABASE
/*
$db = null;
$dbs = array(
  'mothy' => array(
    'DRIVER' => 'mysql',
    'PORT' => 5432,
    'HOST' => '127.0.0.1',
    'NAME' => 'codebook',
    'USER' => 'mothy',
    'PASS' => '100613'
  ),
  'soni' => array(
    'DRIVER' => 'mysql',
    'PORT' => 5432,
    'HOST' => '127.0.0.1',
    'NAME' => 'codebook',
    'USER' => 'root',
    'PASS' => '123'
  ),
  'remote' => array(
    'DRIVER' => 'pgsql',
    'PORT' => 5432,
    'HOST' => 'ec2-54-83-36-203.compute-1.amazonaws.com',
    'NAME' => 'dc8upos3k8720s',
    'USER' => 'bslbbzimlcilyy',
    'PASS' => 'y996XgJVY3S_KlL12K06_EZKh4'
    )
);

$where = exec("hostname",$ouput,$status);
switch ($where) {
  case 'soni':
    $id = 'soni';
    break;
  case 'mothy-PC':
    $id = 'mothy';
    break;
}
//$id = ($where == 'mothy-PC') ? 'local' : 'remote';
try {
  $db = new PDO($dbs[$id]['DRIVER'].":host=".$dbs[$id]['HOST'].";dbname=".$dbs[$id]['NAME'],$dbs[$id]['USER'] ,$dbs[$id]['PASS']);
  $db->exec("SET CHARACTER SET utf8");
  $app->db = $db;
} catch (PDOException $e) {
  echo  $e->getMessage();
}
*/


foreach(glob(MODELS_DIR.'*.php') as $model) {
  Require_once $model;
}
$app->session = $_SESSION;
?>
