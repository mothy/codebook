<?php
namespace Modelos;
//{{{ Db

class Db{
// {{{ properties
/**
 * Objeto tabla a ser utilizado
 * @var object
*/
  protected $tabla;                 // objeto tabla  
/**
 * Nombre de la tabla en la BD
 * @var string
*/
  protected $tname='';                 // string nombre de tabla
/**
 * Límite de registros al paginar
 * @var int
*/
  protected $limite=20;             // Máximo para paginar
/**
 * Lista de campos de la tabla mostrados publicamente, separados por coma
 * @var string
*/
  public $camposPublicos='*';        // Campos públicos para show
/**
 * Lista de campos de la tabla para paginar y campos que serán ignorados después de procesados
 * @var array
*/
  public $_fieldsToPaginate = array('*','');  // Campos para paginación
/**
 * Condición para paginar registros, por defecto 1, es decir, sin condición
 * @var string
*/
  public $paginarDonde='1';       // condicion para paginar
/**
 * Campos de la tabla que no serán mostrados, apun siendo incluidos en las listas de campos
 * @var array
*/
  protected $protectedData = array();
/**
 * Campo que sirve como llave primaria e identificador de cada registro por defecto 'id'
 * @var string
*/
  protected $id='id';               // id predeterminado
/**
 * Objeto que almacena la conexión la BD y sus tablas como métodos y propiedades
 * @var object
*/
  protected $db;                    // objeto base de datos
/**
 * Arreglo que contiene cadena de texto con los campos hijos, el alias a usarse y el campo de ordenamiento 
 * @var array
*/
  protected $_has=array();          // objeto contiene hijos
/**
 * Cadena que almacena el nombre del campo y tipo de ordenamiento de los registros 
 * @var string
*/
  protected $orden='';              // cadena ordena por
/**
 * Objeto que almacena la conexión PDO a la base de datos
 * @var object
*/
  protected $pdo=null;              // objeto pdo para consultas Raw
  /**
   * Resultado de las consultas puede ser Json o Array
   * @var array
   * @access protected
   */  
  protected $response = null;       //respuesta del modelo, json o array
// }}}
//{{{ conecta()
/**
 * Recibe el nombre de la tabla a usar
 * Invoca la conexión a la BD
 * Asigna la tabla como objeto con métodos y propiedaes
 * Asigna el nombre de la tabla como cadena de texto
 * @param string t Nombre de la tabla en la base de datos
 * @access public 
 */
  public function conecta($t){
    $this->db = $this->_conexion();
    $this->tabla = $this->db->$t();
    $this->tname = $t;
  }//conecta
//}}}

//{{{ _conexion()
/**
 * Realiza la conexión a la BD usando las constantes previamente declaradas
 * Define el modo de caracteres como utf-8
 * Define como se mostrarán los métodos de errores pdo
 * Invoca a la clase NotORM  
 * @access private 
 */
  private function _conexion(){
    $this->pdo = new \PDO("mysql:dbname=".DBNAME.";host=".DBHOST, DBUSER,DBPASS);
    $this->pdo->exec("SET CHARACTER SET utf8");
    $this->pdo->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING );
    return new \NotORM($this->pdo);
  }
//}}}
//{{{ guardar()
/**
 * Almacena los datos en la tabla, recibe un arreglo con los datos
 * @param array Arreglo que contiene los datos a ser registrados
 * @return array Devuelve los datos registrados o un arreglo vacío si hubo error
 */
  public function guardar($datos){
    if($data = $this->tabla->insert($datos)){
      return $data;
    }
    return array();
  }
//}}}

//{{{ guardar()
/**
 * Elimina un registro de la tabla
 * Si hay más registros relacionados en otra tabla, son también elimnados
 * Devuelve 1 si fue satisfactoria la eliminación, 0 si no lo fue
 * @param int Id del registro a ser eliminado
 * @return int
 */
  public function borrar($id){
    if($d = $this->tabla->where("{$this->tname}.{$this->id}={$id}")){

      if($d->delete()){
        if(isset($this->_has[0]) and $this->_has[0]!=''){        
          $a = '\\Modelos\\'.ucwords($this->_has[0]);
          $c = new $a();
          $e = $c->tabla->where("{$c->tname}.{$this->tname}_{$this->id} = $id");
          $e->delete();
        }
        echo 1;
        }else{
            echo 0;
        }

      }else{
        echo 0;
    }
  }
  //}}}

  public function actualizar($id,$datos){
    if($d=$this->tabla->where("{$this->tname}.{$this->id}={$id}")){
      #echo $d;
      if($data = $d->update($datos)){
        return $data;
      }else{
        echo 0;
      }
    }

  }

  public function muestraDonde($fields='*', $cond='1',$order=''){
    $datos = array();
    if($order!=''){if($data = $this->tabla->select($fields)->where($cond)->order($order)){$datos = $data;}      
    }elseif($data = $this->tabla->select($fields)->where($cond)){$datos= $data;}
    return $datos;
  }
  

  function todos($fields='*', $cond='1',$order=''){
    if($order==''){"{$this->tname}.id desc";}
    if ($data = $this->muestraDonde($fields, $cond, $order)){
      $this->response = $this->iterar($data);
    }
    return json_encode($this->response);
  }

  public function muestraUno($id){//muestraDonde on frontEnd
    if($data = $this->muestraDonde($this->camposPublicos, "{$this->tname}.{$this->id}={$id}")){
     $this->comoJson($this->iterar($data));
    }
 }
 /**
 * @param string $campo El campo a ser buscado
 * @param string $valor El valor a comparar
 * @return int el número de registros encontrados o 0 si no encontró
 * @access public
 **/
 function existe($campo, $valor){
  $field = "count({$this->id}) as existe";
#  if($d = $this->tabla->select($field)->where("UPPER({$campo})","UPPER({$valor})")){
   if($d = $this->tabla->select($field)->where("UPPER($campo) = UPPER(?)",$valor)){
    $datos=array_change_key_case((array)$d[0]->getIterator());
    return $datos['existe'];
  }
}
 
  public function iterar($p){
    $data=array();
    foreach($p as $k=>$v){
        $datos=array_change_key_case((array)$v->getIterator());
        if($this->_fieldsToPaginate[1]!=''){
          $datos = $this->_unset($datos,$this->_fieldsToPaginate[1]);
        }

        if(count($this->protectedData)>0){
          $datos = $this->_unset($datos,$this->protectedData);
        }
       
        $data[]= $datos;
    }

    if(count($data)==1) {
     $datox = $data[0];

     if(isset($this->_has[0]) and $this->_has[0]!=''){        
      $a = '\\Modelos\\'.ucwords($this->_has[0]);
      $c = new $a();
      $c->paginarDonde = $this->tname.'_id=' . $datox['id'];
      if($d = $c->tabla->select($c->_fieldsToPaginate[0])->where($c->paginarDonde)){
        $datox[$this->_has[1]] = $c->iterar($d);
      }
      
      #$datox[$this->_has[1]] = $c->_paginate();
    }
    } 
    else{
      $datox = $data;
    }

    return $datox;
  }  

  function _unset($data,$str){
    $arr = (gettype($str)=='string')?explode(',',$str):$str;
    
    foreach($arr as $k=>$v){
        $v = trim($v);
        if(isset($data[$v])){unset($data[$v]);}
    }
    return $data;
  }

  function respondeComoJson(){
    $this->comoJson($this->response);
  }

  function respondeComoArreglo(){
    return $this->response;
  }

  function comoJson($data){
    header('Access-Control-Allow-Origin: *');
    header('Cache-Control: no-cache, must-revalidate');
    header('Content-type: application/json');    
    echo json_encode($data, JSON_NUMERIC_CHECK);
  } 

  function camposparaPaginar($f='*',$d=''){
    if($this->tname!=''){
      $e = explode(',', $f);
      foreach($e as $k=>$v){
        $l[] = trim($v);
        $c[] = $this->tname.'.'.trim($v);
      }
      $a = implode(',',$c);
    }
    $this->_fieldsToPaginate = array($a, $d);
    #pre($this->_fieldsToPaginate);
  }

  function _paginate($i=1, $f=20){
    $totalRows=$this->count();
    $last_page=ceil($totalRows/$f);
    $next_page=($i<$last_page)?$i+1:$last_page;
    $prev_page=($i>1)?$i-1:1;

    $orden=($this->orden=='')?$this->tname.'.'.$this->id.' desc':$this->orden;
    
    if(!empty($this->paginarDonde)){
      $datos=$this
      ->tabla
      ->select($this->_fieldsToPaginate[0])
      ->where($this->paginarDonde)
      ->order($orden)
      ->limit($f,$f*($i-1));
    }else{
      $datos=$this
      ->tabla
      ->select($this->_fieldsToPaginate[0])
      ->order($orden)
      ->limit($f,$f*($i-1));      
    }

    $data=array(
      'total'=>$totalRows,
      'per_page'=>$f,
      'current_page'=>$i,
      'last_page'=>$last_page,
      'next_page'=>$next_page,
      'prev_page'=>$prev_page,
      'datos'=>$this->iterar($datos)
    );  
    return $data;
  }

  function paginar($i=1, $f=20){
    $a = $this->_paginate($i, $f);
    $this->asJson($a);
  }

  function count(){
    $tabla=$this->tname;
    $condicionao='';
    #echo $this->paginarDonde;
    #$condicionao = ($this->paginarDonde=='' or $this->paginarDonde=='1')?'':$this->paginarDonde;
    if(($this->paginarDonde=='' or $this->paginarDonde=='1')){
      $cuantos = $this->db->$tabla()->select('count('.$this->id.') as total');
    }else{
      $cuantos = $this->db->$tabla()->select('count('.$this->id.') as total')->where($this->paginarDonde);
    }
    #echo $cuantos;
    $a = (array)$cuantos[0]->getIterator();
    return $a['total'];
  }

  public function perteneceA($tabla,$campos,$alias='_perteneceA'){
    $bel=array();
    $fieldsTemp=array();
    $publicTemp=array();

    if(count($campos)>0){
      foreach($campos as $k=>$v){
        if(is_numeric($k)){array_push($bel, $tabla.'.'.$v." as {$alias}_{$v}");}
        else{array_push($bel, $tabla.'.'.$k." as {$alias}_{$v}");}
      }
      #pre($this->_fieldsToPaginate[0]);
      if($this->_fieldsToPaginate[0]=='*' and count($bel)>0){
        $this->_fieldsToPaginate[0]=$this->tname.'.*,'.implode(',',$bel);
      }else{
        $bang = explode(',',$this->_fieldsToPaginate[0]);
        $b = array_merge($bang, $bel);
        $this->_fieldsToPaginate[0]=implode(',',$b);
      }
      #pre($this->camposPublicos);
      if($this->camposPublicos=='*' and count($bel)>0){
        $this->camposPublicos=$this->tname.'.*,'.implode(',',$bel);
      }else{
        $bangs = explode(',',$this->camposPublicos);
        $aux=array();
        foreach($bangs as $k=>$v){          
          if(strpos($v,'.')===false){
            array_push($aux, $this->tname.'.'.$v);
          }else{
            array_push($aux, $v);
          }
        }
        $this->camposPublicos=implode(',', $aux).','.implode(',',$bel);
      } 
      #pre($this->_fieldsToPaginate, $this->camposPublicos);       
    }
  }

  public function tiene($child,$alias='_tiene',$orden=''){
    if($orden==''){
        $ord="$this->tname.$this->id desc";
    }else{
        $ord=$orden;
    }
    $this->_has=array($child,$alias,$ord);
  }
}
//}}}
