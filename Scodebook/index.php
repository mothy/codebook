<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
header('Access-Control-Max-Age: 86400');
error_reporting(E_ALL);
ini_set('display_errors', 1);
require __DIR__.'/vendor/autoload.php';
require __DIR__.'/config.php';
require __DIR__.'/libs/NotORM.php';


$app->options('/\w+',function() use($app){});
$app->get('/',function() use($app){
  echo file_get_contents('index.html');
})->name('landing');

$app->get('/pruebas',function() use($app){
  $foros = new Modelos\Foros();
  $foritos = json_decode($foros->todos());
  $i = 0;
  $j = 1;
  foreach($foritos as $it){
  	$data[$j][] = $it;
  	$i++;
  	if((($i)%3) === 0){
  		$j++;
  	}
  }
  echo json_encode($data);
})->name('pruebas');

$app->get('/username',function () use($app){
  $res = array('usuario' => 'mothy');
  echo json_encode($res);
})->name('getuser');

$app->post('/registrar',function () use($app){
  $post = json_decode($app->request->getBody(),true);
  $data = ['nombre' => $post['name'],'usuario' => $post['user'],'email' => $post['email'],'clave'=>$post['pass'],'img'=>'hacker.png'];
  $usuarios = new Modelos\Usuarios();
  $existUser = $usuarios->existe('usuario',$post['user']);
  $existEmail = $usuarios->existe('email',$post['email']);
  if($post['name']=="" || $post['email']=="" || $post['pass']=="" || $post['user'] == ""){
    echo 0;
  }else if(!($existUser || $existEmail)){
        $usuarios->guardar($data);
        echo 3;
      }else{
        echo 2;
      }
})->name('registrar');

$app->post('/login',function () use($app){
  $posts = json_decode($app->request->getBody(),true);
  $datas = ['usuario'=>$posts['user'],'clave'=>$posts['pass']];
  $usuarios = new Modelos\Usuarios();
  $pass = $usuarios->muestraDonde('clave,id as indice','usuario="'.$posts['user'].'"');
  if(!is_null($pass) && $posts['pass'] == $pass[0]['clave']){
    echo $pass[0]['indice'];
  }else{
    echo 0;
  } 
})->name('login');

$app->post('/profile',function($id = null) use($app){
  $post = json_decode($app->request->getBody(),true);

  if($post[0] == null){
    die();
  }else{
    $usuario = new Modelos\Usuarios();
  $data = $usuario->muestraUno($post[0]);
  return json_encode($usuario);  
  }
  
})->name('profile');

$app->post('/update',function($id = null) use($app){
  $posts = json_decode($app->request->getBody(),true);
  $usuario = new Modelos\Usuarios();
  $datas = ['nombre'=>$posts['name'],'usuario'=>$posts['user'],'clave'=>$posts['pass'],'email'=>$posts['email']];
  
  if($posts['name']=="" || $posts['email']=="" || $posts['pass']=="" || $posts['user'] == ""){
    echo 2;
  }else {
    $usuario->actualizar($posts['id'],$datas);
    $usuariomismo = $usuario->muestraDonde('usuario','id="'.$posts['id'].'"');
    if($usuariomismo[0]['usuario'] == $posts['user']){
      $usuario->actualizar($posts['id'],$datas);
    }else{
      $existUsers = $usuario->existe('usuario',$posts['user']);
      if(!($existUsers)){
        $usuario->actualizar($posts['id'],$datas);
        }else{
          echo 1;
        }
    } 
  }
})->name('update');

$app->post('/actualizar',function() use($app){
  $posts = json_decode($app->request->getBody(),true);
  $usuario = new Modelos\Usuarios();
  $datas = ['img'=>$posts['img']];
  $up = $usuario->actualizar($posts['id'],$datas);
  if($up == 1){
    echo 1;
  }else{
    echo 0;
  }
})->name('actualizar');

$app->post('/new',function() use($app){
  $posts = json_decode($app->request->getBody(),true);
  $hilos = new Modelos\Hilos();
  
  $date = new DateTime();
  $data = $datas = ['titulo'=>$posts['titulo'],'contenido'=>trim($posts['contenido']), 'status'=>'N','usuarios_id'=>'1', 'foros_id'=>$posts['id'],'fecha'=>$date->format('Y-m-d H:i:s')];
  $hilos->guardar($data);
})->name('new');

$app->post('/getposts',function() use($app){
  $posts = json_decode($app->request->getBody(),true);
  $hilos = new Modelos\Hilos();
  $data = $hilos->muestraDonde('*','foros_id="'.$posts['id'].'"');
  echo json_encode($data);
})->name('getposts');

$app->post('/new_comentario',function() use($app){
  $posts = json_decode($app->request->getBody(),true);
  $hilos = new Modelos\Comentarios();
  $date = new DateTime();
  $datas = ['cuerpo'=>$posts['cuerpo'],'status'=>$posts['status'],'fecha'=>$date->format('Y-m-d H:i:s'),'usuarios_id'=>$posts['usuarios_id']];
  $hilos->guardar($datas);
})->name('new_comentario');

$app->post('/get_comentarios',function() use($app){
  $posts = json_decode($app->request->getBody(),true);
  $hilos = new Modelos\Hilos();
  var_dump($posts);
  die();
  $data = $hilos->muestraDonde('*','foros_id="'.$posts['id'].'"');
  echo json_encode($data);
})->name('get_comentarios');
$app->run();
?>