<?php
	error_reporting(E_ALL | E_WARNING | E_NOTICE);
	ini_set('display_errors', TRUE);
	date_default_timezone_set('America/Mexico_City');
	session_start();

	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: *');
	header('Access-Control-Allow-Headers: *');

	#define('PREFIX','emsvys');
	define('DBNAME','tecvalles');
	define('DBHOST','localhost');
	define('DBUSER','root');
	define('DBPASS','');

	function pre(){
		$args = func_get_args();
		foreach($args as $k=>$v){
			echo '<pre>';
			print_r($v);
			echo '</pre>';
		}
	}
?>